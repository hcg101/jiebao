#!/bin/sh
sudo echo "Starting..."
#MKSQSHFS4='/home/hcg/OpenWrt-SDK-15.05-ramips-mt7620_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64/staging_dir/host/bin/mksquashfs4'
#PADJFFS2='/home/hcg/OpenWrt-SDK-15.05-ramips-mt7620_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64/staging_dir/host/bin/padjffs2'
MKSQSHFS4='./bin/mksquashfs_xz-4.0'
PADJFFS2='./bin/padjffs2'
unsquashfs='./bin/unsquashfs_xz-4.0'
USER='hcg'
case "$1" in
'extract'|'e')
offset1=`grep -oba hsqs $2 | grep -oP '[0-9]*(?=:hsqs)'`
offset2=`wc -c $2 | grep -oP '[0-9]*(?= )'`
size2=`expr $offset2 - $offset1`
#echo $offset1 " " $offset2 " " $size2
sudo rm -fr kernel.bin
dd if=$2 of=kernel.bin bs=1 ibs=1 count=$offset1
dd if=$2 of=secondchunk.bin bs=1 ibs=1 count=$size2 skip=$offset1
sudo rm -rf squashfs-root 2>&1
sudo $unsquashfs -d squashfs-root secondchunk.bin
rm secondchunk.bin
sudo chown -R $USER squashfs-root/
;;
'create'|'c')
sudo $MKSQSHFS4 ./squashfs-root ./newsecondchunk.bin -nopad -noappend -root-owned -b 256k -processors 1
sudo chown $USER ./newsecondchunk.bin
cat kernel.bin newsecondchunk.bin > $2
$PADJFFS2 $2
sudo rm -fr newsecondchunk.bin
#sudo rm -fr kernel.bin
;;
*)
echo 'run
"modify-firmware.sh extract firmware.bin"
You will find file "kernel.bin" and folder "squashfs-root".
Modify "squashfs-root" as you like,after everything is done,run
"modify-firmware.sh create newfirmware.bin"
And you will get a modified firmware named newfirmware.bin.
'
;;
esac
